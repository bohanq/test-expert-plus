var ClickEvents = (function () {
  var instance;

  function InnerClickEvents () {
    var self = this;
    self.eventsList = {};
    document.body.addEventListener('click', function (event){

      if (typeof self.allClick === 'function') {
        self.allClick(event.target);
      }

      if (event.path !== undefined && event.path.length > 0) {
        event.path.forEach(function (elem) {
          
          if (elem.nodeType === 1 && (eventName = elem.getAttribute('data-event')) !== null) {
            self.dispatchEvent(eventName, elem);
          }
        });
      }
    });
  };
  
  InnerClickEvents.prototype.addEventListener = function (eventName, callBack) {
    var self = this;

    if (eventName !== undefined && eventName !== null && typeof callBack === 'function') {
      self.eventsList[eventName] = callBack;
    }
  };

  InnerClickEvents.prototype.addEventAllClick = function (callBack) {
    var self = this;

    if (typeof callBack === 'function') {
      self.allClick = callBack;
    }
  };

  InnerClickEvents.prototype.removeEventAllClick = function () {
    var self = this;
    delete self.allClick
  };

  InnerClickEvents.prototype.dispatchEvent = function (eventName, elem) {
    var self = this;

    if (typeof self.eventsList[eventName] === 'function') {
      self.eventsList[eventName](elem);
    }
  };

  return {
    'getInstance' : function () {
      if (!instance) {
        instance = new InnerClickEvents;
      }

      return instance; 
    }
  };

})();