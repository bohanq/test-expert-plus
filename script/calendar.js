function Calendar (params) 
{
  var params = params || {};
  this.dayNames = ["Понедельник","Вторник","Среда","Четверг","Пятница", "Суббота", "Воскрисенье"];
  this.monthNames = ["Январь","Февраль","Март","Апрель","Май","Июнь","Июль","Август","Сентябрь","Октябрь","Ноябрь", "Декабрь"];
  this.monthLiteNames = ["января","февраля","марта","апреля","мая","июня","июля","августа","сентября","октября","ноября", "декабря"];
  this.currentDate = new Date();
  this.currentDate = new Date(this.currentDate.getFullYear(), this.currentDate.getMonth(), this.currentDate.getDate());
  this.contentConatiner = document.getElementById('calendar-content');
  this.contentSource = document.getElementById("calendar-template").innerHTML;
  this.contentDetails = document.getElementById("calendar-details").innerHTML;
  this.searchContent = document.getElementById("search-content").innerHTML;
  this.date = new Date(this.currentDate.getFullYear(), this.currentDate.getMonth(), 1);
  this.viewDate = document.querySelector('nav .view-date');
  this.eventsData = params.eventsData;
  this.renderContent(new Date(this.currentDate.getFullYear(), this.currentDate.getMonth(), 1));
};

    Calendar.prototype.nextMonth = function () 
    {
      var self = this;
      self.date.setMonth(self.date.getMonth() + 1);
      self.renderContent(new Date(self.date.getFullYear(), self.date.getMonth(), 1));
    };

    Calendar.prototype.prevMonth = function () 
    {
      var self = this;
      self.date.setMonth(self.date.getMonth() - 1);
      self.renderContent(new Date(self.date.getFullYear(), self.date.getMonth(), 1));
    };

    Calendar.prototype.currentMonth = function () 
    {
      var self = this;
      self.date = new Date(self.currentDate.getFullYear(), self.currentDate.getMonth(), 1)
      self.renderContent(new Date(self.currentDate.getFullYear(), self.currentDate.getMonth(), 1));
    };
    
    Calendar.prototype.renderContent = function (date) 
    {
      var self = this;
      var weekFirstDay = date.getDay();

      if (weekFirstDay > 1 && weekFirstDay < 7) {
        date.setDate((weekFirstDay-2)*(-1));
      }

      var days = [];

      while (days.length < 35) {
        var dateString = ("0" + date.getDate()).slice(-2) + '.' + ("0" + (date.getMonth()+1)).slice(-2) + '.' + date.getFullYear();
        days.push({
            'dayTitle' : days.length <= 6 ? self.dayNames[days.length] + ', ' + date.getDate() : date.getDate(),
            'clouseWeek' : (days.length+1) % 7 === 0,
            'currentDay' : date.getTime() === self.currentDate.getTime(),
            'dateString' : dateString,
            'event' : self.eventsData[dateString] 
        });
        date.setDate(date.getDate()+1);
      }

      var template = Handlebars.compile(self.contentSource);
      self.contentConatiner.innerHTML = template({'days' : days});
      self.renderNav();
    };

    Calendar.prototype.renderNav = function () 
    {
      var self = this;

      if (self.viewDate !== null) {
        self.viewDate.innerHTML = self.monthNames[self.date.getMonth()] + ' ' + self.date.getFullYear(); 
      }
    };

    Calendar.prototype.getDetails = function (dateString) 
    {
      var self = this;
      var template = Handlebars.compile(self.contentDetails);
      var dateSplit = dateString.split('.');
      var date = new Date(dateSplit[2], (dateSplit[1]-1), dateSplit[0]);
      return template({
        'eventDate' : dateString,
        'eventData' : self.eventsData[dateString] || false,
        'userFormatDate' : date.getDate() + ' ' + self.monthLiteNames[date.getMonth()]
      });
    };

    Calendar.prototype.setEvent = function (data) 
    {
      var self = this;
      self.eventsData[data['eventDate']] = {
        'name' : data['eventName'],
        'members' : data['eventMambers'],
        'description' : data['eventDescription']
      };
      
      self.renderContent(new Date(self.date.getFullYear(), self.date.getMonth(), 1));
    };

    Calendar.prototype.editEvent = function (data) {
      var self = this;
      self.eventsData[data['eventDate']].description = data['eventDescription'];

      if (data['eventMambers']) {
        self.eventsData[data['eventDate']].members = data['eventMambers'];
      }
      
      self.renderContent(new Date(self.date.getFullYear(), self.date.getMonth(), 1));
    };
    
    Calendar.prototype.removeEvent = function (dateString) 
    {
      var self = this;
      delete self.eventsData[dateString];
      self.renderContent(new Date(self.date.getFullYear(), self.date.getMonth(), 1));
    };

    Calendar.prototype.setEventLite = function (dateString) 
    {
      var self = this;
      var splitTitle = dateString.split(", ");
      var splitDate = splitTitle[0].split(" ");
      splitTitle.splice(0,1);
      var date = new Date(self.date.getFullYear(), self.monthLiteNames.indexOf(splitDate[1]), splitDate[0]);
      self.setEvent({
        'eventDate' : ("0" + date.getDate()).slice(-2) + '.' + ("0" + (date.getMonth()+1)).slice(-2) + '.' + date.getFullYear(),
        'eventName' : splitTitle.join(', ')
      })

      self.renderContent(new Date(self.date.getFullYear(), self.date.getMonth(), 1));
    };

    Calendar.prototype.search = function (word) 
    {
      var self = this;
      var data = {};

      if (word == '') {
        return false;
      }

      for (var k in self.eventsData) {
        if (self.eventsData[k].name.indexOf(word) > -1) {
          var dateSplit = k.split('.');
          var date = new Date(dateSplit[2], (dateSplit[1]-1), dateSplit[0]);
          data[k] = {
            'name' : self.eventsData[k].name,
            'date' : date.getDate() + ' ' + self.monthLiteNames[date.getMonth()]
          };
        }
      }

      if (Object.keys(data).length == 0) {
        return false;
      } else {
        var template = Handlebars.compile(self.searchContent);
        return template({'result':data});
      }
    };