var clickEvent = ClickEvents.getInstance();
var calendar = new Calendar({'eventsData' : testEventsData});

clickEvent.addEventListener('calendar-prevMonth', function () {
  calendar.prevMonth();
});

clickEvent.addEventListener('calendar-nextMonth', function () {
  calendar.nextMonth();
});

clickEvent.addEventListener('calendar-currentMonth', function () {
  calendar.currentMonth();
});

var popupDetails, liteAdded, searchPopup;

clickEvent.addEventListener('add-calendar-event', function (elem) {
  	var eventDate = elem.getAttribute('data-event-date');

    if (popupDetails === undefined || (popupDetails && !popupDetails.isParent(elem))) {
      	elem.classList.add('focus-day');
	    popupDetails = new Popup({
	        'parent' : elem,
	        'content' : calendar.getDetails(eventDate),
	        'orintationPosition' : 'auto',
	        'class' : 'calendar-details',
	        'clouseCallBack' : function () {
				popupDetails.params.parent.classList.remove('focus-day');
				clickEvent.removeEventAllClick();
				delete popupDetails;
	        }
	    });

	    clickEvent.addEventAllClick(function (elem) {
	      	if (!popupDetails.isChild(elem)) {
	          	popupDetails.clouse();
	        }
      	});
    }
});

//clouse-popup
clickEvent.addEventListener('clouse-popup', function () {
	setTimeout(function() {
		popupDetails.clouse();
	},50);
});

clickEvent.addEventListener('calendar-set-event', function (elem) {
	var elems = elem.parentElement.querySelectorAll('[name]');
	var data = {};
	Array.prototype.forEach.call(elems, function (item) {
		data[item.getAttribute('name')] = item.value;
	});

	calendar.setEvent(data);
});

clickEvent.addEventListener('calendar-edit-event', function (elem) {
	var elems = elem.parentElement.querySelectorAll('[name]');
	var data = {};
	Array.prototype.forEach.call(elems, function (item) {
		data[item.getAttribute('name')] = item.value;
	});

	calendar.editEvent(data);
});

clickEvent.addEventListener('calendar-remove-event', function (elem) {
	var dateString = elem.parentElement.querySelector('[name="eventDate"]').value;
	calendar.removeEvent(dateString);
});

clickEvent.addEventListener('calendar-lite-set-event', function (elem) {
	var eventDateTitle = elem.parentElement.querySelector('[name="eventDateTitle"]').value;
	calendar.setEventLite(eventDateTitle);
	liteAdded.clouse();
});

clickEvent.addEventListener('calendar-lite-added', function (elem) {
    if (liteAdded === undefined || (liteAdded && !liteAdded.isParent(elem))) {
	    liteAdded = new Popup({
	        'parent' : elem.parentElement,
	        'content' : document.getElementById('lite-added').innerHTML,
	        'orintationPosition' : 'bottom',
	        'class' : 'lite-set-event',
	        'clouseCallBack' : function () {
				clickEvent.removeEventAllClick();
				delete liteAdded;
	        }
	    });

	    clickEvent.addEventAllClick(function (elem) {
	      	if (!liteAdded.isChild(elem)) {
	          	liteAdded.clouse();
	        }
      	});
    }
});

var search = document.getElementById('calendar-search');

if (search !== null) {
	search.addEventListener('input', function(event) {
		var searchContent = calendar.search(event.target.value);

		if (searchPopup === undefined && searchContent !== false) {
		    searchPopup = new Popup({
		        'parent' : event.target.parentElement,
		        'content' : searchContent,
		        'orintationPosition' : 'bottom',
		        'class' : 'search',
		        'clouseCallBack' : function () {
					clickEvent.removeEventAllClick();
					searchPopup = undefined;
					console.log(searchPopup);
		        }
		    });

		    clickEvent.addEventAllClick(function (elem) {
		      	if (!searchPopup.isChild(elem)) {
		          	searchPopup.clouse();
		        }
	      	});
    	} else if (searchPopup !== undefined && searchContent !== false) {
    		searchPopup.contentContainer.innerHTML = searchContent;
    	} else if (searchPopup !== undefined && searchContent == false) {
    		searchPopup.clouse();
    	}
	});
}