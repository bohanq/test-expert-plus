function Popup (params) {
	this.params = params || {};
	this.box = document.createElement('div');
	this.box.classList.add('popup'); 

	if (this.params.class) {
		this.box.classList.add(this.params.class); 
	}

	if (this.params.orintationPosition === 'auto') {
		var parentCoordinate = this.params.parent.getBoundingClientRect();
		var position = parentCoordinate.top > (document.documentElement.clientHeight/2) ? 'bottom-' : 'top-';
		position += parentCoordinate.left > (document.documentElement.clientWidth/2) ? 'right' : 'left';
		this.box.classList.add(position);
	} else {
		this.box.classList.add(this.params.orintationPosition);
	}

	this.contentContainer = document.createElement('div');
	this.contentContainer.classList.add('content');
	this.contentContainer.innerHTML = this.params.content;
	this.box.appendChild(this.contentContainer);
	this.params.parent.appendChild(this.box);
};

Popup.prototype.clouse = function () {
	var self = this;
	self.params.parent.removeChild(self.box);

	if (typeof self.params.clouseCallBack === 'function') {
		self.params.clouseCallBack(self);
	}
};
Popup.prototype.isChild = function (elem) {
	var self = this;
	return self.box.contains(elem);
};
Popup.prototype.isParent = function (elem) {
	var self = this;
	return elem.contains(self.box);
};